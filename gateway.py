from concurrent.futures import ThreadPoolExecutor
import lowmsock
import asyncio
import time
import json
import traceback
import logging
import numpy as np
from common.radio import Radio, C
from common.environment import RadioEnv, PathLossExp, Temp
from common.packet import Message, MessageType
from lowmsock import LoWMSession

CLIENTS = []
env = None
executor = ThreadPoolExecutor()

DEFAULT_HOST = '127.0.0.1'
DEFAULT_PORT = 8080
DEFAULT_ENV = RadioEnv(PathLossExp.INDOOR_LOS, Temp.ROOM)

class Client:
    def __init__(self, radio, session: LoWMSession) -> None:
        self.radio = radio
        self.session = session
        self.tx_noise_power = 0
        self.receiving = False

    def corrupt_bits(self, env: RadioEnv, src_radio, msg: bytes, added_noise: float) -> bytes:
        bit_array = np.unpackbits(np.frombuffer(msg, dtype=np.uint8))
        snr = env.get_snr_db(src_radio, self.radio, added_noise)
        gilbert_p = min(max(0.0038 * snr * snr - 0.00533 * snr + 0.1843, 0.0), 1.0)

        # p0 = [Pr[Error=1], Pr[Error=0]]
        p0 = np.array([[gilbert_p, 1.0 - gilbert_p]])

        #  T = 0: [ Pr[Ei=0|E(i-1)=0], Pr[Ei=1|E(i-1)=0]
        #      1: [ Pr[Ei=0|E(i-1)=1], Pr[Ei=1|E(i-1)=1]
        gilbert_r = 0.00601795
        T = np.array([[1.0 - gilbert_p, gilbert_p], [gilbert_r, 1.0 - gilbert_r]])
        for i in range(0, len(msg)):
            p0 = np.dot(p0, T)
            if np.random.rand() > p0[0][0]:
                p0 = np.array([[0.0, 1.0]])
            else:
                p0 = np.array([[1.0, 0.0]])
                bit_array[i] = 1.0 - bit_array[i]
        return np.packbits(bit_array).tobytes()

    async def send(self, src_radio, env: RadioEnv, msg: bytes):
        # calculate remaining transmission and propagation time
        dist = Radio.get_distance(self.radio, src_radio)
        transmit_delay = (len(msg) * 8) / src_radio[Radio.DATA_RATE.value]
        propagation_delay = dist / C

        # message begins to arrive after propagation delay
        await asyncio.sleep(propagation_delay)
        power = env.get_signal_power(src_radio, self.radio)
        snr = env.get_snr_db(src_radio, self.radio, self.tx_noise_power)

        # add up received signal power from other sources
        self.tx_noise_power += power
        if self.receiving or snr < self.radio[Radio.RX_SENSITIVITY.value]:
            # if we are receiving from more than one source, discard others
            # ... or if the signal is too feint, discard it
            await asyncio.sleep(transmit_delay)
            self.tx_noise_power -= power
            return

        self.receiving = True

        # wait for message to fully transmit

        loop = asyncio.get_event_loop()

        msg, _ = await asyncio.gather(*[
            # run corruption async, bit fiddling is resource intensive
            loop.run_in_executor(executor, self.corrupt_bits, env, src_radio, msg, self.tx_noise_power - power),
            # make sure that we wait out the transmission delay, at the very least
            asyncio.sleep(transmit_delay)
        ])

        # transmission over, reset state
        self.tx_noise_power -= power
        self.receiving = False

        # deliver message to client
        await self.session.send(MessageType.MESSAGE_HEADER_PACKET, msg)

async def handle_client(session: LoWMSession):
    client = None
    try:
        while msg := await session.recv():
            msg = bytes(msg)
            if Message.get_type(msg) == MessageType.MESSAGE_HEADER_RADIO:
                try:
                    radio_data = str(Message.get_content(msg), 'utf8')
                    radio = json.loads(radio_data)
                    if client is not None:
                        CLIENTS.remove(client)
                    client = Client(radio, session)
                    CLIENTS.append(client)
                except (json.JSONDecodeError, UnicodeDecodeError) as e:
                    print(f"Received malformed radio data from client {session.session.address}: {msg}")
            elif Message.get_type(msg) == MessageType.MESSAGE_HEADER_PACKET and client is None:
                await session.send(MessageType.MESSAGE_HEADER_NORADIO, b'Client must specify radio before transmitting packets.')
            elif Message.get_type(msg) == MessageType.MESSAGE_HEADER_PACKET:
                await asyncio.gather(*[ c.send(client.radio, env, msg) for c in CLIENTS if c != session ])
    except Exception as _:
        logging.error(traceback.format_exc())
    finally:
        if client is not None:
            CLIENTS.remove(client)

async def main(host: str, port: int, _env: RadioEnv):
    global env
    env = _env
    async with lowmsock.serve(handle_client, host, port) as s:
        print(f'Successfully started gateway server on {host}:{port}.')
        await s

if __name__ == '__main__':
    asyncio.run(main(DEFAULT_HOST, DEFAULT_PORT, DEFAULT_ENV))