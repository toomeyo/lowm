import asyncio
from asyncio import AbstractEventLoop, CancelledError
from contextlib import asynccontextmanager
import logging
from multiprocessing import Event
import socket
import traceback
from typing import Callable
import unittest

class TCPSession:
    def __init__(self, loop: AbstractEventLoop, conn: socket, addr) -> None:
        self.loop = loop
        self.connection = conn
        self.address = addr

    async def recv(self, size: int) -> bytes:
        return await self.loop.sock_recv(self.connection, size)

    async def recv_into(self, buffer: bytearray):
        return await self.loop.sock_recv_into(self.connection, buffer)

    async def send(self, data: bytes):
        await self.loop.sock_sendall(self.connection, data)

    def close(self):
        self.connection.close()

async def _callback_wrapper(callback: Callable[[TCPSession], None], session: TCPSession) -> None:
    try:
        await callback(session)
        session.close() 
    except socket.error:
        # socket was already closed
        pass
    except Exception as _:
        logging.error(traceback.format_exc())

async def _listen_for_connection(callback: Callable[[TCPSession], None], socket: socket, loop: AbstractEventLoop) -> None:
    try:
        while True:
            conn, addr = await loop.sock_accept(socket)
            logging.info("New connection from {}".format(addr))
            session = TCPSession(loop, conn, addr)
            conn.setblocking(False)
            asyncio.create_task(_callback_wrapper(callback, session))
    except asyncio.CancelledError:
        # application layer wants to stop
        raise

def _handle_exceptions(ctx):
    msg = ctx.get('execption', ctx['message'])
    logging.error(f'Caught Exception: {msg}')

@asynccontextmanager    
async def serve(callback: Callable[[TCPSession], None], host: str, port: int):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(False)
    sock.bind((host, port))
    sock.listen()

    logging.info("Server started on {}:{}".format(host, port))

    loop = asyncio.get_event_loop()
    loop.set_exception_handler(_handle_exceptions)

    fut = asyncio.create_task(_listen_for_connection(callback, sock, loop))

    yield fut

    if not fut.done():
        fut.cancel()
        try:
            # give everything a chance to shut down
            await fut
        except CancelledError:
            pass
    sock.close()

@asynccontextmanager
async def connect(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setblocking(False)
    
    loop = asyncio.get_event_loop()
    await loop.sock_connect(sock, (host, port))
    session = TCPSession(loop, sock, (host, port))

    yield session

    try:
        session.close()
    except socket.error:
        # session already closed
        pass

#########################################

class Tests(unittest.TestCase):
    DEFAULT_HOST = '127.0.0.1'
    DEFAULT_PORT = 8080

    @unittest.skip("internal")
    async def connection(self, session: TCPSession):
        while data := await session.recv(1024):
            logging.info("Got data: {}".format(data.decode('ascii')))
            await session.send(data)

    @unittest.skip("internal")
    async def client(self):
        async with connect(Tests.DEFAULT_HOST, Tests.DEFAULT_PORT) as s:
            msg = b'Foo'
            await s.send(msg)
            resp = await s.recv(1024)
            self.assertEquals(msg, resp, 'received message does not match message sent.')
            
    @unittest.skip("internal")
    async def main():
        async with serve(Tests.connection, Tests.DEFAULT_HOST, Tests.DEFAULT_PORT) as w:
            await Tests.client()
            await w

    def test(self):
        logging.getLogger().setLevel(logging.INFO)
        asyncio.run(self.main(), debug=True)