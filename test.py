import asyncio
from email import message
import time
import numpy as np
import unittest
from common.environment import RadioEnv, PathLossExp, Temp
from common.radio import Radio
from lowmsock import connect
from gateway import main

DEFAULT_HOST = '127.0.0.1'
DEFAULT_PORT = 8080

class CRCTests(unittest.IsolatedAsyncioTestCase):
    messages = [
        "The quick brown fox jumps over the lazy dog",
        "Human kind cannot bear very much reality",
        "The fault, dear Brutus, is not in our stars, but in ourselves."
    ]

    polynomial = 0x07

    RADIO_PROPS = {
        Radio.TX_FREQUENCY.value: 2_400_000_000,
        Radio.RX_FREQUENCY.value: 2_400_000_000,
        Radio.TX_POWER.value: 0.1,
        Radio.RX_SENSITIVITY.value: -100,
        Radio.BANDWIDTH.value: 10_000_000,      # 10 MHz
        Radio.DATA_RATE.value: 125_000,         # 125 kbits/s
        Radio.POSITION.value: (0.0, 0.0),       
    }

    async def sender(i: int):
        async with connect(DEFAULT_HOST, DEFAULT_PORT) as client:
            for n, v in CRCTests.RADIO_PROPS.items():
                client.set_property(n, v)

            await client.apply_radio_props()

            await asyncio.sleep(1)
            data = CRCTests.messages[i].encode('ascii')
            crc = CRCTests.crc8(data, CRCTests.polynomial)
            print(f'Sending bits "{CRCTests.messages[i]}" with CRC-8 {crc}.')
            await client.send(crc.to_bytes(1, 'big') + CRCTests.messages[i].encode('ascii'))

    async def receiver():
        async with connect(DEFAULT_HOST, DEFAULT_PORT) as client:
            for n, v in CRCTests.RADIO_PROPS.items():
                client.set_property(n, v)

            await client.apply_radio_props()

            msg = await client.recv()
            crc = CRCTests.crc8(msg[1:], CRCTests.polynomial).to_bytes(1, 'big')
            if crc != msg[0]:
                print(f"Packet was corrupted: CRC-8 is {crc}, expected {msg[0]}")
            else:
                print(f"Packet was received successfully: CRC-8 is {crc}, expected {msg[0]}")

    async def test_crc_demo(self):
        receiver = asyncio.create_task(CRCTests.receiver())

        for i in range(0, len(CRCTests.messages)):
            await CRCTests.sender(i)
        
        await asyncio.sleep(4)
        receiver.cancel()
        try:
            await receiver
        except asyncio.CancelledError:
            pass

    def test_crc(self):
        data = bytearray(b'Foo bar baz bing')
        crc1 = CRCTests.crc8(data, CRCTests.polynomial)
        data[5] += 1 if data[5] != 255 else -255
        crc2 = CRCTests.crc8(data, CRCTests.polynomial)
        self.assertNotEqual(crc1, crc2)
        data[5] += -1 if data[5] != 0 else 255
        crc3 = CRCTests.crc8(data, CRCTests.polynomial)
        self.assertEqual(crc1, crc3)

    def crc8(data: bytes, poly: int) -> int:
        """Calculate 8-bit CRC for input 'data' using polynomial 'poly'"""
        gen = 1 << 8 | poly
        crc = 0

        for b in data:
            crc ^= b

            for _ in range(0, 8):
                crc = crc << 1

                if crc & (1 << 8):
                    crc ^= gen
        
        return crc

class ScaleTest(unittest.IsolatedAsyncioTestCase):

    RADIO_PROPS = {
        Radio.TX_FREQUENCY.value: 2_400_000_000,
        Radio.RX_FREQUENCY.value: 2_400_000_000,
        Radio.TX_POWER.value: 0.1,
        Radio.RX_SENSITIVITY.value: -100,
        Radio.BANDWIDTH.value: 10_000_000,      # 10 MHz
        Radio.DATA_RATE.value: 125_000,         # 125 kbits/s
        Radio.POSITION.value: (0.0, 0.0, 0.0),       # choose this randomly later
    }

    async def sender(self):
        async with connect(DEFAULT_HOST, DEFAULT_PORT) as client:
            for n, v in ScaleTest.RADIO_PROPS.items():
                client.set_property(n, v)

            await client.apply_radio_props()

            await asyncio.sleep(2)
            data = b'Can everyone hear me?'
            print(f'Sending bits "{data}.')
            await client.send(data)

    async def receiver(self):
        async with connect(DEFAULT_HOST, DEFAULT_PORT) as client:
            for n, v in ScaleTest.RADIO_PROPS.items():
                client.set_property(n, v)

            client.set_position((np.random.rand() * 30 - 15, np.random.rand() * 30 - 15, 0.0))

            await client.apply_radio_props()

            msg = await asyncio.wait_for(client.recv(), 5)
            self.assertIsNotNone(msg)


    async def test_scaling(self):
        receivers = asyncio.gather(*[
            self.receiver() for _ in range(0, 100)
        ])

        await self.sender()

        await asyncio.sleep(10)

        receivers.cancel()
        try:
            await receivers
        except asyncio.CancelledError:
            pass

if __name__ == '__main__':
    unittest.main()