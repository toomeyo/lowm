import enum
import math
import numpy as np
from common.radio import Radio

class PathLossExp(enum.Enum):
    FREE_SPACE = (2.0, 0.0)
    URBAN_AREA_CELLULAR = (3.1, 0.4)
    SHADOWED_URBAN_CELL = (4,0, 1.0)
    INDOOR_LOS = (1.7, 0.1)
    INDOOR_OBSTRUCTED = (5.0, 1.0)
    FACTORY_OBSTRUCTED = (2.5, 0.5)

class Temp(enum.Enum):
    FREEZING = 273.15
    ROOM = 293
    COLD_DAY = 277.15
    HOT_DAY = 313
    VENUS_SURFACE = 748.15
    PLUTO_SURFACE = 43.15

class RadioEnv:
    def __init__(self, loss_exp, temp) -> None:
        if isinstance(loss_exp, PathLossExp):
            self.loss_exp = loss_exp.value
        else:
            self.loss_exp = loss_exp
        if isinstance(temp, Temp):
            self.temp = temp.value
        else:
            self.temp = temp
        self.noise_sources = []
        self.add_noise_src(lambda rx: Radio.get_thermal_noise(rx, self.temp))

    def add_noise_src(self, src):
        self.noise_sources.append(src)

    def get_signal_power(self, tx, rx) -> float:
        if rx[Radio.RX_FREQUENCY.value] != tx[Radio.TX_FREQUENCY.value]:
            return 0
        dist = Radio.get_distance(rx, tx)
        exp = self.loss_exp[0] + (2 * np.random.rand() - 1) * self.loss_exp[1]
        return tx[Radio.TX_POWER.value] * Radio.get_loss_factor(dist, rx[Radio.RX_FREQUENCY.value], exp)

    def get_snr_db(self, tx, rx, rx_noise: float) -> float:
        if rx[Radio.RX_FREQUENCY.value] != tx[Radio.TX_FREQUENCY.value]:
            return 0
        signal = self.get_signal_power(tx, rx)
        noise = sum([ func(rx) for func in self.noise_sources ]) + rx_noise
        return 10 * math.log10((signal / noise))
