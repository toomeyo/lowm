import math
import enum

C = 299_792_458 # m/s
K = 1.38064852e-23 # J/K

# modelled radio properties
class Radio(enum.Enum):
    TX_FREQUENCY = 'tx_frequency'
    RX_FREQUENCY = 'rx_frequency'
    TX_POWER = 'transmit_power'
    RX_SENSITIVITY = 'sensitivity'
    BANDWIDTH = 'bandwidth'
    DATA_RATE = 'data_rate'
    POSITION = 'position'

    def get_distance(radio1, radio2) -> float:
        # get the distance between the two specified radios
        p1 = radio1[Radio.POSITION.value]
        p2 = radio2[Radio.POSITION.value]
        vec = (p1[0] - p2[0], p1[1] - p2[1], p1[2] - p2[2])
        return math.sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2])

    def get_loss_factor(dist: float, frequency: float, path_loss_exp: float) -> float:
        wave_len = C / frequency
        if dist == 0:
            return 1
        return math.pow(wave_len / (4 * math.pi * dist), path_loss_exp)

    def get_thermal_noise(radio, temp: float) -> float:
        return K * radio[Radio.BANDWIDTH.value] * temp

class HWRadio:
    def __init__(self) -> None:
        self.props = {}

    def set_tx_freq(self, tx_freq: float) -> 'HWRadio':
        self.props[Radio.TX_FREQUENCY.value] = tx_freq
        return self

    def set_rx_freq(self, rx_freq: float) -> 'HWRadio':
        self.props[Radio.RX_FREQUENCY.value] = rx_freq
        return self

    def set_tx_power(self, tx_power: float) -> 'HWRadio':
        self.props[Radio.TX_POWER.value] = tx_power
        return self

    def set_rx_sensitivity(self, rx_sensitvity: float) -> 'HWRadio':
        self.props[Radio.RX_SENSITIVITY.value] = rx_sensitvity
        return self

    def set_bandwidth(self, bandwidth: float) -> 'HWRadio':
        self.props[Radio.BANDWIDTH.value] = bandwidth
        return self

    def set_data_rate(self, data_rate: float) -> 'HWRadio':
        self.props[Radio.DATA_RATE.value] = data_rate
        return self

    def set_position(self, pos) -> 'HWRadio':
        self.props[Radio.POSITION.value] = pos
        return self

    def set_property(self, prop: str, value):
        self.props[prop] = value
