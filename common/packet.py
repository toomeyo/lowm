import enum

class MessageType(enum.Enum):
    MESSAGE_HEADER_PACKET = b'PCKT'
    MESSAGE_HEADER_RADIO = b'RADI'
    MESSAGE_HEADER_MALFORMED = b'BADP'
    MESSAGE_HEADER_NORADIO = b'NORA'

class Message():
    def to_message(type: MessageType, length: int, data: bytes) -> bytes:
        header = type.value[:4]
        length_bits = length.to_bytes(4, 'big', signed=False)
        return header + length_bits + data

    def get_type(data: bytes) -> MessageType:
        try:
            return MessageType(data[:4])
        except ValueError:
            return None

    def get_length(data: bytes) -> int:
        return int.from_bytes(data[4:8], 'big', signed=False)

    def get_content(data: bytes) -> bytes:
        length = Message.get_length(data)
        if length + 8 > len(data):
            return None
        return data[8:8+length]