# LoWM

Local Area emulating Wireless Mesh (LoWM). Have process on your LAN broadcast to each other through an API that models wireless transmission.


# Setup
Ensure that you have a very new copy of Python 3 installed. LoWM makes use of of features only available in newer version of Python.

# Tests
LoWM uses the `unitest` package to performing testing. To run all tests, perform the following steps:

1. Start the gateway server. If the server is already running this step can be skipped.
```
python3 gateway.py
```
```
2. Run all tests with:
python3 test.py
```
All tests should pass with errors.

# Use
LoWM uses a gateway server to facilitate broadcasting between mesh nodes. Start the gateway server using the following:
```
python3 gateway.py
```
The default port is `33000`, and the default environment is indoors LOS at room temperature. The following example script
demonstrates how to run the server using a different configuration:
```
import asyncio
from gateway import main
from common.environment import RadioEnv, PathLossExp, Temp

HOST = '127.0.0.1'
PORT = 8080
ENV = RadioEnv(PathLossExp.FACTORY_OBSTRUCTED, Temp.HOT_DAY)

asyncio.run(main(HOST, PORT, ENV))
```

See `tests.py` for examples on how to write a client.