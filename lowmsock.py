import asyncio
from contextlib import asynccontextmanager
from typing import Callable
import tcpsocks
from tcpsocks import TCPSession
import logging
import json
from common.radio import HWRadio
from common.packet import Message, MessageType

class LoWMSessionHandler:
    def __init__(self, callback) -> None:
        self.callback = callback

    async def handle_connection(self, sess: TCPSession):
        session = LoWMSession(sess)
        await self.callback(session)

class LoWMSession:
    def __init__(self, session: TCPSession) -> None:
        self.session = session
        self.buffer = bytearray(1024)

    async def recv(self) -> memoryview:
        view = memoryview(self.buffer)
        size = 0
        while True:
            num_bytes = await self.session.recv_into(view)
            # nothing more coming
            if num_bytes == 0:
                return None
            # make sure we recognize the message type
            if Message.get_type(self.buffer) is None:
                logging.warn("Unknown packet type received: ", self.buffer[:num_bytes])
                view = memoryview(self.buffer)
                await self.send(MessageType.MESSAGE_HEADER_MALFORMED, f'Unknown message header received: {self.buffer[0:4]}'.encode('ascii'))
                continue

            # if the message is done, return it
            size += num_bytes
            if Message.get_length(self.buffer) <= size:
                return memoryview(self.buffer)[0:size]

            # update the view, and if the buffer runs out of space increase it in size
            view = view[size:]
            if len(view) == 0 and len(self.buffer) < 8192:
                self.buffer += bytearray(1024)
                view = memoryview(self.buffer)[size:]

    async def send(self, type: MessageType, data: bytes):
        msg = Message.to_message(type, len(data), data)
        await self.session.send(msg)

class LoWMClient(HWRadio):
    def __init__(self, session: LoWMSession) -> None:
        super().__init__()
        self.session = session

    async def recv(self) -> memoryview:
        return await self.session.recv()

    async def send(self, data: bytes):
        await self.session.send(MessageType.MESSAGE_HEADER_PACKET, data)

    async def apply_radio_props(self):
        await self.session.send(MessageType.MESSAGE_HEADER_RADIO, json.dumps(self.props).encode('utf8'))

@asynccontextmanager
async def serve(callback: Callable[[LoWMSession], None], host: str, port: int):
    handler = LoWMSessionHandler(callback)
    async with tcpsocks.serve(handler.handle_connection, host, port) as s:
        yield s

@asynccontextmanager
async def connect(host: str, port: int):
    async with tcpsocks.connect(host, port) as session:
        yield LoWMClient(LoWMSession(session))
